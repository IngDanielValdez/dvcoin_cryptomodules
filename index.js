const SHA256 = require('crypto-js/sha256');

class Transaction {
    constructor(fromAddress, toAddress, amount) {
        this.fromAddress = fromAddress;
        this.toAddress = toAddress;
        this.amount = amount;
    }
}

class Block {
    constructor(timestamp, transactions, previousHash = '') {
        this.timestamp = timestamp;
        this.transactions = transactions;
        this.previousHash = previousHash;
        this.hash = this.calculateHash();
        this.nonce = 0;
    }
    calculateHash() {
        return SHA256(this.index + this.previousHash + this.timestamp + JSON.stringify(this.data) + this.nonce).toString();
    }

    mineBlock(difficulty) {
        while (this.hash.substring(0, difficulty) !== Array(difficulty + 1).join("0")) {
            this.nonce++;
            this.hash = this.calculateHash();
        }
        console.log('Block mined:' + this.hash);
    }
}

class BlockChain {
    constructor() {
        this.chain = [this.createGenesisBlock()];
        this.difficulty = 5;
        this.pendingTransactions = [];
        this.miningReward = 100;
    }

    createGenesisBlock() {
        return new Block('10/04/2021', 'Genesis Block', '0');
    }

    getLatestBlock() {
        return this.chain[this.chain.length - 1];
    }

    minePendingTransactions(miningRewardAddress) {
        let block = new Block(Date.now(), this.pendingTransactions);
        block.mineBlock(this.difficulty);

        console.log('Block successfully mined!');
        this.chain.push(block);

        this.pendingTransactions = [
            new Transaction(null, miningRewardAddress, this.miningReward)
        ];

    }

    createTransaction(transactions) {
        this.pendingTransactions.push(transactions);
    }

    getBalanceOfAddress(address) {
        let balance = 0;

        for (const block of this.chain) {
            for (const trans of block.transactions) {
                if (trans.fromAddress === address) {
                    balance -= trans.amount;
                }

                if (trans.toAddress === address) {
                    balance += trans.amount;
                }
            }
        }

        return balance;
    }

    // addBlock(newBlock) {
    //     newBlock.previousHash = this.getLatestBlock().hash;
    //     // newBlock.hash = newBlock.calculateHash();
    //     newBlock.mineBlock(this.difficulty);
    //     this.chain.push(newBlock);
    // }

    isChainValid() {
        for (let i = 1; i < this.chain.length; i++) {
            const currentBlock = this.chain[i];
            const previousBlock = this.chain[i - 1];

            if (currentBlock.hash !== currentBlock.calculateHash()) {
                return false;
            }

            if (currentBlock.previousHash !== previousBlock.hash) {
                return false;
            }
        }

        return true;
    }
}

let DVCoin = new BlockChain();

DVCoin.createTransaction(new Transaction('address1', 'address2', 100));
DVCoin.createTransaction(new Transaction('address2', 'address1', 50));

console.log('\n Starting the miner...');
DVCoin.minePendingTransactions('daniel-valdez');

console.log('\nBalance of Daniel is', DVCoin.getBalanceOfAddress('daniel-valdez'));

console.log('\n Starting the miner again...');
DVCoin.minePendingTransactions('daniel-valdez');

console.log('\nBalance of Daniel is', DVCoin.getBalanceOfAddress('daniel-valdez'));
// console.log('Block 1');
// DVCoin.addBlock(new Block(1, '19/05/2021', { amount: 10 }));

// console.log('Block 2');
// DVCoin.addBlock(new Block(2, '19/05/2021', { amount: 100 }));

// console.log('Is blockchain valid' + ' ' + DVCoin.isChainValid());

// DVCoin.chain[1].data = { amount: 100 };
// DVCoin.chain[1].hash = DVCoin.chain[1].calculateHash();

// console.log('Is blockchain valid' + ' ' + DVCoin.isChainValid());

// console.log(JSON.stringify(DVCoin, null, 4));